package com.kshrd.adminController;

import com.kshrd.model.Product;
import com.kshrd.model.Stock;
import com.kshrd.model.StockDetail;
import com.kshrd.service.admin.category.StockDetailService;
import com.kshrd.service.admin.category.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/admin")
public class StockController {

    @Autowired
    StockService stockService;

    @Autowired
    StockDetailService stockDetailService;

    @GetMapping("/getStockHistory")
    @ResponseBody
    public String getProductByCate(ModelMap m)
    {

        m.addAttribute("data",stockService.getStockHistory());
        System.out.println(m);
        return "";
    }

    @GetMapping("/getStockDetailByStcd")
    @ResponseBody
    public Map<String,Object> getStockDetailByStcd(@ModelAttribute("stcd") String stcd)
    {
        System.out.println(stcd+"kak");
        Map<String,Object> respone= new HashMap<>();
        respone.put("data",stockDetailService.getStockDetailByStcd(stcd));

        System.out.println(respone);

      return respone;

    }
    @PostMapping("/insertStock")
    public String insertStock(@RequestBody HashMap<String, Object> inRec){

        StockDetail detail=new StockDetail();
        Product product=new Product();
        Stock stock=new Stock();

        ArrayList arrIn = (ArrayList) inRec.get("IN_REC");

        System.out.println(arrIn);

        String stcd =   "STC"+UUID.randomUUID();
        stock.setStcd(stcd);
        stockService.insertToStock(stcd);

            for (int i = 0; i < arrIn.size(); i++) {
                HashMap item = (HashMap) arrIn.get(i);

                String prcd = (String) item.get("prcd");
                Double stQty = Double.parseDouble(item.get("new-qty")+"");
                Double stockPrice = Double.parseDouble(item.get("price-instock")+"");

                product.setPrcd(prcd);
                detail.setStPricd(stockPrice);
                detail.setStQty(stQty);
                detail.setProduct(product);
                detail.setStock(stock);

                stockDetailService.insertToStockDetail(detail);

            }

        return "redirect:/admin/stock";
    }


}
