package com.kshrd.adminController;

import com.kshrd.model.OrderDetail;
import com.kshrd.model.Orders;
import com.kshrd.service.admin.category.OrderDetailservice;
import com.kshrd.service.admin.category.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/admin")
public class OrderDetailController {

    @Autowired
    OrderDetailservice ordersService;

    @GetMapping("/getAllItem")
    @ResponseBody
    public List<OrderDetail> getAll(@ModelAttribute("orderCd") String orderCd){
        //map.addAttribute("data",ordersService.getAllItem(orderCd));
        return ordersService.getAllItem(orderCd);
    }


}
