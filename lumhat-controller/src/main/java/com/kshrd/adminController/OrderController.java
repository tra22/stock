package com.kshrd.adminController;

import com.kshrd.model.OrderDetail;
import com.kshrd.model.Orders;
import com.kshrd.model.Product;
import com.kshrd.repository.adminReposity.OrderDetailRepository;
import com.kshrd.repository.adminReposity.OrderRepository;
import com.kshrd.service.admin.category.OrderDetailservice;
import com.kshrd.service.admin.category.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/admin")
public class OrderController {

    @Autowired
    OrdersService ordersService;

    @Autowired
    OrderDetailservice orderDetailservice;

    @PostMapping("/saveEstimation")
    @ResponseBody
    public String order(@RequestBody HashMap<String, Object> inRec){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        ArrayList arrIn = (ArrayList) inRec.get("IN_REC");
        float amount= Float.parseFloat(inRec.get("TOTAL")+"");

        System.out.println(amount);
        String orderCd =   UUID.randomUUID()+"-"+dateFormat.format(date);
        String userCd =   UUID.randomUUID()+"";

        Orders order = new Orders(orderCd,amount,"not yet trandsaction money",false,userCd);
        ordersService.insert(order);

         for (int i = 0; i < arrIn.size(); i++) {
            HashMap item = (HashMap) arrIn.get(i);
            String prcd = (String) item.get("prcd");
            int qty = Integer.parseInt(item.get("qty")+"");
            float unitPrice = Float.parseFloat(item.get("total")+"");

            OrderDetail orderDetail = new OrderDetail(qty,unitPrice,orderCd,prcd);

            orderDetailservice.insert(orderDetail);
        }

        return "sucess";
    }

    @PostMapping("/payment")
    @ResponseBody
    public String payment(@RequestBody HashMap<String, Object> inRec){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        ArrayList arrIn = (ArrayList) inRec.get("IN_REC");
        float amount= Float.parseFloat(inRec.get("TOTAL")+"");

        System.out.println(amount);
        String orderCd =   UUID.randomUUID()+"-"+dateFormat.format(date);
        String userCd =   UUID.randomUUID()+"";

        Orders order = new Orders(orderCd,amount,"trandsaction money complete",true,userCd);
        ordersService.insert(order);

        for (int i = 0; i < arrIn.size(); i++) {
            HashMap item = (HashMap) arrIn.get(i);
            String prcd = (String) item.get("prcd");
            int qty = Integer.parseInt(item.get("qty")+"");
            float unitPrice = Float.parseFloat(item.get("total")+"");

            OrderDetail orderDetail = new OrderDetail(qty,unitPrice,orderCd,prcd);

            orderDetailservice.insert(orderDetail);
        }

        return "sucess";
    }

    @GetMapping("/recall")
    @ResponseBody
    public List<Orders> getRecall(){
        ordersService.selectRecall();
        return ordersService.selectRecall();
      }
}
