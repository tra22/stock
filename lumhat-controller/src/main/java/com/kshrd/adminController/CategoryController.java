package com.kshrd.adminController;

import com.kshrd.model.Category;
import com.kshrd.service.admin.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class CategoryController  {

    @Autowired
    private CategoryService service;

    @GetMapping("/getChildCategory")
    @ResponseBody
    public Map<String ,Object> category(@ModelAttribute("parent_select") String parentSelect, ModelMap map)
    {
        Map<String, Object> response = new HashMap<>();
        int parentId= Integer.parseInt(parentSelect);
        response.put("data", service.findCategoryByParentId(parentId));
        System.out.println(response);

        return response;
    }

}
