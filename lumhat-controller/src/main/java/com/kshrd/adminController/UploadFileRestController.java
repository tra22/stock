package com.kshrd.adminController;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/file/upload")
public class UploadFileRestController {

    @Value("${spring.upload.server.path}")
    String serverPath;

    @PostMapping
    public String uploadFile(@RequestParam("file") MultipartFile file, ModelMap m) {
        Map<String, Object> map = new HashMap<>();
        File path=new File(serverPath);
        System.out.println("file: "+file.getOriginalFilename());
        if (!path.exists()) {
            if (path.mkdirs()) {

            } else {
            }
        }
        try {
            String filename=file.getOriginalFilename();
            String extension=filename.substring(filename.lastIndexOf('.')+1);
            filename=UUID.randomUUID()+"."+extension;
            System.out.println(filename);
            Files.copy(file.getInputStream(),Paths.get(serverPath,filename));
            String url="/img/"+filename;
            map.put("data",url);
            map.put("filename", filename);
            map.put("extension", extension);
            map.put("path",path);
            map.put("status",true);
            m.addAttribute("response",filename);

        } catch (Exception e) {
            map.put("size", "cannot upload");
            map.put("status", false);

        }
        return "redirect:/admin/product";
    }



}

