package com.kshrd.adminController;

import com.kshrd.model.Product;
import com.kshrd.model.Stock;
import com.kshrd.repository.adminReposity.ProductRepositoy;
import com.kshrd.service.admin.category.CategoryService;
import com.kshrd.service.admin.category.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/admin")
public class ProductController {

    @Autowired
    private ProductService service;

    @Value("${spring.upload.server.path}")
    String serverPath;

    @GetMapping("/getProductByCate")
    @ResponseBody
    public Map<String ,Object> getProductByCate(@ModelAttribute("catcd") String catcd)
    {
        Map<String, Object> response = new HashMap<>();
        response.put("data", service.getAllProduct(catcd));
        System.out.println("kak.."  +service.getAllProduct(catcd));

        System.out.println("gg"+ catcd);
        return response;
    }

    @GetMapping("/getAllProductStock")
    @ResponseBody
    public Map<String ,Object> getAllProduct()
    {
        Stock stock = new Stock();
        Map<String, Object> response = new HashMap<>();
        response.put("data",service.getAllProduct());
        System.out.println(response);
        return response;
    }

    @PostMapping("/addProduct")
    public String saveProduct(@ModelAttribute Product product,
                              @ModelAttribute("parentId") String parentId,
                              @ModelAttribute("childId") String childId,
                              @RequestParam("file") MultipartFile image)
    {

        Map<String,Object> map=new HashMap<>();

        File path=new File(serverPath);
        System.out.println("file: "+image.getOriginalFilename());
        if (!path.exists()) {
            if (path.mkdirs()) {

            } else {
            }
        }
        try {
            String filename     =   image.getOriginalFilename();
            String extension    =   filename.substring(filename.lastIndexOf('.')+1);
            filename            =   UUID.randomUUID()+"."+extension;
            System.out.println(filename);
            Files.copy(image.getInputStream(),Paths.get(serverPath,filename));
        } catch (Exception e) {
            map.put("size", "cannot upload");
            map.put("status", false);

        }


       /* String prcd =   UUID.randomUUID()+"prcd";
        Stock stock=new Stock();
        Product pro=new Product(prcd,product,cateCd,url,Float.parseFloat(price),stock.setStQty(Integer.parseInt(stockQty)),stock.setProduct(this),
                Integer.parseInt(parentId),Integer.parseInt(childId));
        System.out.println("product"+parentId);*/

        return "redirect:/admin/product";
    }

    @GetMapping("/searchProductByName")
    @ResponseBody
    public Map<String ,Object> searchProductByName(@ModelAttribute("tSearch") String pName)
    {
        Map<String, Object> response = new HashMap<>();
        response.put("data", service.searchProductByName(pName+"%"));
        return response;
    }

    @GetMapping("/getAllProductInStock")
    @ResponseBody
    public Map<String ,Object> getAllProductInStock()
    {
        Map<String, Object> response = new HashMap<>();
        response.put("data", service.getAllProductInStock());
        return response;
    }


}
