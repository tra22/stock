package com.kshrd.adminController;
import com.kshrd.repository.adminReposity.CategoryRepositoy;
import com.kshrd.service.admin.category.CategoryService;
import com.kshrd.service.admin.category.OrdersService;
import com.kshrd.service.admin.category.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin")
public class AdminHomeController {

    @Autowired
    private CategoryService service;

    @Autowired
    OrdersService ordersService;

    @Autowired
    StockService stockService;

    @GetMapping("/userManagement")
    public String userManagement( ){

        return "admin/index";
    }

    @GetMapping("/dashboard")
    public String dashboad( ){
        return "/admin/dashboard";
    }

    @GetMapping("/stock")
    public String stock(ModelMap m){
        m.addAttribute("data",stockService.getStockHistory());
        System.out.println(m);
        return "/admin/stock";
    }


    @GetMapping("/order")
    public String order(ModelMap map){
        map.addAttribute("category",service.getAllCategory());
        map.addAttribute("recalls",ordersService.selectRecall());
        return "/admin/order";
    }

    @GetMapping("/product")
    public String product(ModelMap map){
        map.addAttribute("category",service.getAllCategory());
        return "/admin/product";
    }

    @GetMapping("/upload")
    public String file_test(){
        return "/admin/file_upload";
    }


}
