package com.kshrd.service.admin.category;
import com.kshrd.model.Category;
import com.kshrd.model.Orders;
import com.kshrd.repository.adminReposity.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersServicImp implements OrdersService {

    @Autowired
    OrderRepository repository;

    @Override
    public void insert(Orders order) {
        repository.insert(order);
    }

    @Override
    public List<Orders> selectRecall() {
        return repository.selectRecall();
    }
}
