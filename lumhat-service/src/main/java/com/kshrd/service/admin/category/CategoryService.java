package com.kshrd.service.admin.category;

import com.kshrd.model.Category;

import java.util.List;

public interface CategoryService {

    public List<Category> getAllCategory();

    public List<Category> findCategoryByParentId(int id);
}
