package com.kshrd.service.admin.category;

import com.kshrd.model.StockDetail;
import com.kshrd.repository.adminReposity.StockDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockDetaileServiceImp implements StockDetailService {

    @Autowired
    StockDetailRepository stockDetailRepository;

    @Override
    public void insertToStockDetail(StockDetail stockDetail) {
        stockDetailRepository.insertToStockDetail(stockDetail);
    }

    @Override
    public List<StockDetail> getStockDetailByStcd(String stcd) {
        return stockDetailRepository.getStockDetailByStcd(stcd);
    }
}
