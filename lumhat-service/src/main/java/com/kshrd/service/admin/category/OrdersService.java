package com.kshrd.service.admin.category;

import com.kshrd.model.Orders;

import java.util.List;

public interface OrdersService {
   public void insert(Orders order);
   List<Orders> selectRecall();
}
