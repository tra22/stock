package com.kshrd.service.admin.category;

import com.kshrd.model.Category;
import com.kshrd.model.Product;
import com.kshrd.model.Stock;
import com.kshrd.repository.adminReposity.ProductRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServicImp implements ProductService {

    @Autowired
    ProductRepositoy productRepositoy;

    @Override
    public List<Product> getAllProduct(String catcd) {
        return productRepositoy.findAllProducts(catcd);
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepositoy.getAllProduct();
    }

    @Override
    public List<Product> searchProductByName(String name) {
        return productRepositoy.searchProductByName(name);
    }

    @Override
    public List<Product> getAllProductInStock() {
        return productRepositoy.getAllProductInStock();
    }
}
