package com.kshrd.service.admin.category;

import com.kshrd.model.OrderDetail;
import com.kshrd.model.Orders;
import com.kshrd.repository.adminReposity.OrderDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderDetailServiceImp implements OrderDetailservice {

    @Autowired
    OrderDetailRepository repository;

    @Override
    public void insert(OrderDetail detail) {
        repository.insert(detail);
    }

    @Override
    public List<OrderDetail> getAllItem(String orderCd) {
        return repository.getAllItem(orderCd);
    }
}
