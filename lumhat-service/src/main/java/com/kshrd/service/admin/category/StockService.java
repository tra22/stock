package com.kshrd.service.admin.category;

import com.kshrd.model.Stock;
import com.kshrd.model.StockDetail;

import java.util.List;

public interface StockService {

    public List<Stock> getStockHistory();

    void insertToStock(String stcd);

}
