package com.kshrd.service.admin.category;

import com.kshrd.model.Product;
import com.kshrd.model.Stock;

import java.util.List;

public interface ProductService {

    public List<Product> getAllProduct(String catcd);
    public List<Product> getAllProduct();
    List<Product> searchProductByName(String name);
    List<Product> getAllProductInStock();
}
