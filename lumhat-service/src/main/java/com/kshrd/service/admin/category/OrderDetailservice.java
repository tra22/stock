package com.kshrd.service.admin.category;

import com.kshrd.model.OrderDetail;

import java.util.List;

public interface OrderDetailservice {
    void insert(OrderDetail detail);
    public List<OrderDetail> getAllItem(String orderCd);
}
