package com.kshrd.service.admin.category;

import com.kshrd.model.Category;
import com.kshrd.repository.adminReposity.CategoryRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServicImp implements CategoryService {

    @Autowired
    CategoryRepositoy categoryRepositoy;

    @Override
    public List<Category> getAllCategory() {
        return categoryRepositoy.findAllCategory();
    }

    @Override
    public List<Category> findCategoryByParentId(int id) {
        return categoryRepositoy.findCategoryByParentId(id);
    }
}
