package com.kshrd.service.admin.category;

import com.kshrd.model.StockDetail;

import java.util.List;

public interface StockDetailService {

    void insertToStockDetail(StockDetail stockDetail);
    List<StockDetail> getStockDetailByStcd(String stcd);

}
