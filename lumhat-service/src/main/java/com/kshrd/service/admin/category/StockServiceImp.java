package com.kshrd.service.admin.category;

import com.kshrd.model.Stock;
import com.kshrd.model.StockDetail;
import com.kshrd.repository.adminReposity.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockServiceImp implements StockService {

    @Autowired
    public StockRepository repository;


    @Override
    public List<Stock> getStockHistory() {

        return repository.getStockHistory();
    }

    @Override
    public void insertToStock(String stcd) {
        repository.insertToStock(stcd);
    }
}
