package com.kshrd.model;

public class Stock {
    public int id;
    public String stcd;
    public Supplier supplier;
    public int stQty;
    public String stInDate;
    public String remark;
    public Product product;
    public User user;

    public Stock(String stcd, Supplier supplier, int stQty, String stInDate, String remark, Product product, User user) {
        this.stcd = stcd;
        this.supplier = supplier;
        this.stQty = stQty;
        this.stInDate = stInDate;
        this.remark = remark;
        this.product = product;
        this.user = user;
    }

    public String getStInDate() {
        return stInDate;
    }

    public void setStInDate(String stInDate) {
        this.stInDate = stInDate;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Stock() {
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", stcd='" + stcd + '\'' +
                ", supplier=" + supplier +
                ", stQty=" + stQty +
                ", stInDate=" + stInDate +
                ", remark='" + remark + '\'' +
                ", product=" + product +
                ", user=" + user +
                '}';
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStcd() {
        return stcd;
    }

    public void setStcd(String stcd) {
        this.stcd = stcd;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public int getStQty() {
        return stQty;
    }

    public void setStQty(int stQty) {
        this.stQty = stQty;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
