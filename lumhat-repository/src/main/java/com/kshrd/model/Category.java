package com.kshrd.model;

public class Category {
    public int cateId;
    public int parentId;
    public String nmEng;
    public String lvl;
    public String userCd;
    public String regDate;
    public String catgCd;
    public String catgParent;
    public int vsCatGrid;

    public int getCategoryId() {
        return cateId;
    }

    public void setCategoryId(int cateId) {
        this.cateId = cateId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getNmEng() {
        return nmEng;
    }

    public void setNmEng(String nmEng) {
        this.nmEng = nmEng;
    }

    public String getLvl() {
        return lvl;
    }

    public void setLvl(String lvl) {
        this.lvl = lvl;
    }

    public String getUserCd() {
        return userCd;
    }

    public void setUserCd(String userCd) {
        this.userCd = userCd;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getCatgCd() {
        return catgCd;
    }

    public void setCatgCd(String catgCd) {
        this.catgCd = catgCd;
    }

    public String getCatgParent() {
        return catgParent;
    }

    public void setCatgParent(String catgParent) {
        this.catgParent = catgParent;
    }

    public int getVsCatGrid() {
        return vsCatGrid;
    }

    public void setVsCatGrid(int vsCatGrid) {
        this.vsCatGrid = vsCatGrid;
    }

    public Category(int cateId, int parentId, String nmEng, String lvl, String userCd, String regDate, String catgCd, String catgParent, int vsCatGrid) {
        this.cateId = cateId;
        this.parentId = parentId;
        this.nmEng = nmEng;
        this.lvl = lvl;
        this.userCd = userCd;
        this.regDate = regDate;
        this.catgCd = catgCd;
        this.catgParent = catgParent;
        this.vsCatGrid = vsCatGrid;
    }

    public Category() {
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + cateId +
                ", parentId=" + parentId +
                ", nmEng='" + nmEng + '\'' +
                ", lvl='" + lvl + '\'' +
                ", userCd=" + userCd +
                ", regDate='" + regDate + '\'' +
                ", catgCd='" + catgCd + '\'' +
                ", catgParent='" + catgParent + '\'' +
                ", vsCatGrid=" + vsCatGrid +
                '}';
    }
}
