package com.kshrd.model;

public class Orders {
    public String orderCd;
    public double total;
    public String remark;
    public boolean status;
    public String userCd;

    public Orders() {
    }

    public Orders(String orderCd, double total, String remark, boolean status, String userCd) {
        this.orderCd = orderCd;
        this.total = total;
        this.remark = remark;
        this.status = status;
        this.userCd = userCd;
    }

    public String getOrderCd() {
        return orderCd;
    }

    public void setOrderCd(String orderCd) {
        this.orderCd = orderCd;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getUserCd() {
        return userCd;
    }

    public void setUserCd(String userCd) {
        this.userCd = userCd;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "orderCd='" + orderCd + '\'' +
                ", total=" + total +
                ", remark='" + remark + '\'' +
                ", status=" + status +
                ", userCd='" + userCd + '\'' +
                '}';
    }
}
