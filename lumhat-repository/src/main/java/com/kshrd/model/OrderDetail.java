package com.kshrd.model;

public class OrderDetail {
 public int id;
 public int qty;
 public double unitPrice;
 public String orderCd;
 public String title;
 public double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String prcd;

    public OrderDetail() {
    }

    public OrderDetail(int id, int qty, double unitPrice, String orderCd, String title, String prcd) {
        this.id = id;
        this.qty = qty;
        this.unitPrice = unitPrice;
        this.orderCd = orderCd;
        this.title = title;
        this.prcd = prcd;
    }

    public OrderDetail(int qty, double unitPrice, String orderCd, String prcd) {
        this.qty = qty;
        this.unitPrice = unitPrice;
        this.orderCd = orderCd;
        this.prcd = prcd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getOrderCd() {
        return orderCd;
    }

    public void setOrderCd(String orderCd) {
        this.orderCd = orderCd;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrcd() {
        return prcd;
    }

    public void setPrcd(String prcd) {
        this.prcd = prcd;
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
                "id=" + id +
                ", qty=" + qty +
                ", unitPrice=" + unitPrice +
                ", orderCd='" + orderCd + '\'' +
                ", title='" + title + '\'' +
                ", prcd='" + prcd + '\'' +
                '}';
    }
}
