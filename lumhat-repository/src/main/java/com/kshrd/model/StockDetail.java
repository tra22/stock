package com.kshrd.model;

public class StockDetail {

    public int id;
    public Stock stock;
    public Double stQty;
    public Double stPrice;
    public User user;
    public Product product;

    public StockDetail() {
    }

    public StockDetail(Stock stock, Double stQty, Double stPrice, User user, Product product) {
        this.stock = stock;
        this.stQty = stQty;
        this.stPrice = stPrice;
        this.user = user;
        this.product = product;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Double getStQty() {
        return stQty;
    }

    public void setStQty(Double stQty) {
        this.stQty = stQty;
    }

    public Double getStPrice() {
        return stPrice;
    }

    public void setStPricd(Double stPrice) {
        this.stPrice = stPrice;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "StockDetail{" +
                "id=" + id +
                ", stock=" + stock +
                ", stQty=" + stQty +
                ", stPricd=" + stPrice +
                ", user=" + user +
                ", product=" + product +
                '}';
    }

}
