package com.kshrd.model;

public class Product {
    public int prid;
    public String prcd;
    public String title;
    public String catgcd;
    public String url;
    public float price;
    public Stock stock;
    public int parentId;
    public int childId;
    public float totalStockQty;
    public float stockPrice;

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Product(String prcd, String title, String catgcd, String url, float price, Stock stock, int parentId, int childId, float totalStockQty, float stockPrice) {
        this.prcd = prcd;
        this.title = title;
        this.catgcd = catgcd;
        this.url = url;
        this.price = price;
        this.stock = stock;
        this.parentId = parentId;
        this.childId = childId;
        this.totalStockQty = totalStockQty;
        this.stockPrice = stockPrice;
    }

    public Product(int prid, String prcd, String title, String catgcd, String url, float price) {
        this.prid = prid;
        this.prcd = prcd;
        this.title = title;

        this.catgcd = catgcd;
        this.url = url;
        this.price = price;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    public int getPrid() {
        return prid;
    }

    public void setPrid(int prid) {
        this.prid = prid;
    }

    public String getPrcd() {
        return prcd;
    }

    public void setPrcd(String prcd) {
        this.prcd = prcd;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCatgcd() {
        return catgcd;
    }

    public void setCatgcd(String catgcd) {
        this.catgcd = catgcd;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Product() {
    }

    @Override
    public String toString() {
        return "Product{" +
                "prid=" + prid +
                ", prcd='" + prcd + '\'' +
                ", title='" + title + '\'' +
                ", catgcd='" + catgcd + '\'' +
                ", url='" + url + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                ", parentId=" + parentId +
                ", childId=" + childId +
                ", totalStockQty=" + totalStockQty +
                ", stockPrice=" + stockPrice +
                '}';
    }

    public float getTotalStockQty() {
        return totalStockQty;
    }

    public void setTotalStockQty(float totalStockQty) {
        this.totalStockQty = totalStockQty;
    }

    public float getStockPrice() {
        return stockPrice;
    }

    public void setStockPrice(float stockPrice) {
        this.stockPrice = stockPrice;
    }
}
