package com.kshrd.repository.adminReposity;

import com.kshrd.model.Category;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepositoy {

    @Select("SELECT catgid,nm_eng,catgcd,catgparent from category")
    @Results({
                @Result(property = "cateId",column = "catgid"),
                @Result(property = "catgCd",column = "catgcd"),
                @Result(property = "nmEng",column = "nm_eng"),
                @Result(property = "catgParent",column = "catgparent")
            })
    List<Category> findAllCategory();

    @Select("select catgid,nm_eng,catgcd,catgparent\n" +
            "  from category\n" +
            " where parentid = #{id}\n" +
            " and lvl='2'")
    @Results({
            @Result(property = "cateId",column = "catgid"),
            @Result(property = "nmEng",column = "nm_eng"),
            @Result(property = "catgParent",column = "catgparent")
    })
    List<Category> findCategoryByParentId(int id);
}
