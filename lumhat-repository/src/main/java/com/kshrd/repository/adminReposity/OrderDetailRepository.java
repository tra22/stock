package com.kshrd.repository.adminReposity;

import com.kshrd.model.OrderDetail;
import com.kshrd.model.Orders;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailRepository {

    @Insert("insert into order_detail (qty,unit_price,ordercd,prcd) VALUES(#{qty},#{unitPrice},#{orderCd},#{prcd})")
    void insert(OrderDetail detail);

    @Select("select d.qty,d.unit_price,p.title,d.prcd,d.ordercd,p.price from order_detail  d inner join products p ON d.prcd = p.prcd where ordercd=#{orderCd}")
    @Results({
            @Result(property = "unitPrice",column = "unit_price"),
            @Result(property = "orderCd",column = "ordercd")
    })
    public List<OrderDetail> getAllItem(String orderCd);

}
