package com.kshrd.repository.adminReposity;

import com.kshrd.model.Orders;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository {

    @Insert("insert into orders (ordercd,total,remark,usercd,status) VALUES(#{orderCd},#{total},#{remark},#{userCd},#{status})")
    void insert(Orders order);

    @Select("select * from orders where status=false")
    List<Orders> selectRecall();

}
