package com.kshrd.repository.adminReposity;


import com.kshrd.model.Stock;
import com.kshrd.model.StockDetail;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockDetailRepository {

    @Select("select " +
            "d.st_qty," +
            "d.st_price," +
            "p.title," +
            "p.url," +
            "p.price from" +
            " stock_detail" +
            " d  inner join" +
            " products p on d.prcd = p.prcd " +
            "and d.stcd=#{stcd}")
    @Results({
            @Result(property = "stQty",column = "st_qty"),
            @Result(property = "stPrice",column = "st_price"),
            @Result(property = "product.title",column = "title"),
            @Result(property = "product.price",column = "price"),
            @Result(property = "product.url",column = "url"),
    })
    List<StockDetail> getStockDetailByStcd(String stcd);

    @Insert("insert into  stock_detail (stcd,st_qty,st_price,prcd) values (#{stock.stcd},#{stQty},#{stPrice},#{product.prcd})")
    void insertToStockDetail(StockDetail stockDetail);


}
