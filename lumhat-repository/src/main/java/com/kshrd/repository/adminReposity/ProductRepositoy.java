package com.kshrd.repository.adminReposity;

import com.kshrd.model.Category;
import com.kshrd.model.Product;
import com.kshrd.model.Stock;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepositoy {

    @Select("select title,prid,url,price,prcd from products where catgcd=#{catcd}")
    List<Product> findAllProducts(String catcd);

    @Select("select p.prcd,p.price,p.title,s.st_qty,p.url from products p inner join  io_stock s on p.prcd= s.prcd ")
    @Results({
            @Result(property = "stock.stQty",column = "st_qty")
    })
    List<Product> getAllProduct();

    @Insert("insert into products (prcd,regdate,title,price,url,catgcd,parentid,childid) values(#{prcd},now(),#{title},#{price},#{url},#{catgcd},#{parentId},#{childId})")
    Product addProduct(Product pro);

    @Select("select p.prcd,p.price,p.title,s.st_qty,p.url from products p inner join  io_stock s on p.prcd= s.prcd where p.title like #{name}")
    @Results({
            @Result(property = "stock.stQty",column = "st_qty")
    })
    List<Product> searchProductByName(String name);


    @Select("\n" +
            " select p.title,\n" +
            "    p.prcd,\n" +
            "    p.url,\n" +
            "    p.price,\n" +
            "    (select sum(st_qty)  from stock_detail where prcd=p.prcd group by prcd) as total_stock_qty,\n" +
            "    (select avg(d.st_price) from stock_detail d where d.prcd=p.prcd  group by prcd) as  stock_price\n" +
            " from products p;")
    @Results({
            @Result(property = "totalStockQty",column = "total_stock_qty"),
            @Result(property = "stockPrice",column = "stock_price")
    })
    List<Product> getAllProductInStock();
}















