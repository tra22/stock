package com.kshrd.repository.adminReposity;


import com.kshrd.model.Stock;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockRepository {

    @Select("select\n" +
            "   s.st_in_date,\n" +
            "   s.remark,\n" +
            "   s.stcd,\n" +
            "   u.username\n" +
            "from stock s inner join \"user\" u on s.usercd = u.usercd")
    @Results({
            @Result(property = "user.name",column = "username"),
            @Result(property = "stInDate",column = "st_in_date")
    })
    List<Stock> getStockHistory();


    @Insert("insert into stock (stcd,st_in_date,usercd) values (#{stcd},now(),'13234354fgfdfher1')")
    void insertToStock(String stcd );



}
